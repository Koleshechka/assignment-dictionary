%include "colon.inc"
%include "words.inc"
%include "lib.inc"


%define BUFFER_SIZE 256

section .data

length_error: db "Uncorrect length.", 0
unknown_key_error: db "Key not found.", 0
input_string: db "Write your key: ", 0
print_answer: db "Value is: ", 0

section .text

extern find_word

global _start
_start:
	mov rdi, input_string
	call print_string
.check_length:
	sub rsp, BUFFER_SIZE
	mov rdi, rsp
	mov rsi, BUFFER_SIZE
	call read_word
	test rax, rax
	jz .if_too_long

	mov rsi, LABEL
	mov rdi, rsp
	call find_word
	test rax, rax
	jz .if_unknown_key

	add rax, 8
    	mov rdi, rax
    	push rdi
    	call string_length
    	pop rdi
    	add rdi, rax
    	inc rdi
	push rdi
	mov rdi, print_answer
	call print_string
	pop rdi
.end:
	add rsp, BUFFER_SIZE
	call print_string
	call exit
.if_unknown_key:
	mov rdi, unknown_key_error
	jmp .end
.if_too_long:
	mov rdi, length_error
	jmp .end
