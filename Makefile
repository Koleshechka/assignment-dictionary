ELF = -felf64

.PHONY: package
package: main clean
main: main.o dict.o lib.o
	ld -o main main.o dict.o lib.o

main.o: main.asm
	nasm $(ELF) -o main.o main.asm

dict.o: dict.asm
	nasm $(ELF) -o dict.o dict.asm

lib.o: lib.asm
	nasm $(ELF) -o lib.o lib.asm

clean:
	rm -rf *.o
