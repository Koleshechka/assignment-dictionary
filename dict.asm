%include "lib.inc"

global find_word

section .text

find_word:
	push rdi
	.loop:
		test rsi, rsi
		jz .not_found
		add rsi, 8
		push rsi
		call string_equals
		pop rsi
		test rax, rax
		jnz .found
		mov rsi, [rsi - 8]
		jmp .loop
	.not_found:
		mov rsi, 8
	.found:
		mov rax, rsi
		sub rax, 8
		pop rdi
		ret
		
