global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global string_copy
global read_char
global read_word
global parse_uint
global parse_int
s
%define SYS_READ 0
%define SYS_WRITE 1
%define SYS_EXIT 60
%define stdin 0
%define stdout 1
%define stderr 2

section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, SYS_EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
	xor rax, rax
.loop:
	cmp byte [rdi+rax], 0
	je .end
	inc rax
	jmp .loop
.end:
	ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	call string_length
	mov rsi, rdi
	mov rdx, rax
	mov rdi, stdout
	mov rax, SYS_WRITE
	syscall
	ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
	mov rdi, 10

; Принимает код символа и выводит его в stdout
print_char:
	push rdi
	mov rax, SYS_WRITE
	mov rdx, 1
	mov rdi, stdout
	mov rsi, rsp
	syscall
	pop rdi
    ret


; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
	cmp rdi, 0
	jns .print
	push rdi
	mov rdi, '-'
	call print_char
	pop rdi
	neg rdi
.print:

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	push rdx
	mov r10, 10
	mov r9, rsp
	mov rax, rdi
	dec rsp
	mov byte [rsp], 0
.loop:
	mov rdx, 0
	div r10
	add rdx, 48
	dec rsp
	mov byte [rsp], dl
	cmp rax, 0
	jnz .loop
	mov rdi, rsp
	call print_string
	mov rsp, r9
	pop rdx
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    mov rcx, 0
.loop:
	mov dl, [rdi + rcx]
	mov dh, [rsi + rcx]
	cmp dl, dh
	jne .notequals
	inc rcx
	test dl, dl
	jne .loop
	mov rax, 1
	ret
.notequals:
	mov rax, 0
	ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	push 0
	mov rsi, rsp
	mov rax, SYS_READ
	mov rdi, stdin
	mov rdx, 1
	syscall
	pop rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
	push rdi
	push r9
	push r10
	mov r9, rdi
	mov r10, rsi
.read_whitespace:
	call read_char
	cmp rax, 0x20
	je .read_whitespace
	cmp rax, 0x9
	je .read_whitespace
	cmp rax, 0xA
	je .read_whitespace
	cmp rax, 0
	je .error
.loop:
	cmp rax, 0x0
	je .end
	cmp rax, 0x20
	je .end
	cmp rax, 0x9
	je .end
	cmp rax, 0xA
	je .end
	dec r10
	cmp r10, 0
	jbe .error
	mov byte [r9], al
	inc r9
	call read_char
	jmp .loop

.end:
	mov byte [r9], 0
	pop r10             
	pop r9             
	mov rdi, [rsp]      
	call string_length
	mov rdx, rax        
	pop rax             
	ret

.error:
	pop r10             
	pop r9
	pop rdi
	mov rax, 0
	mov rdx, 0
	ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось



parse_uint:
	mov r8, 10		
	xor rcx, rcx		
	xor rax, rax		
.loop:    
	movzx r9, byte[rdi + rcx]	
	cmp r9b, 0			
 	je .save
 	cmp r9b, '0'
 	jb .save
	cmp r9b, '9'
	ja .save
	mul r8			
	sub r9b, '0'		
	add rax, r9		
	inc rcx			
	jmp .loop		
.save:
	mov rdx, rcx		
	ret








; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
	mov rax, 0
	cmp byte[rdi], 45
	je .neg
	call parse_uint
	ret
.neg:
	inc rdi
	call parse_uint
	neg rax
	inc rdx
    ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
	push rdi
	push rsi
	push rdx
	call string_length
	cmp rax, rdx
	ja .end
	mov rcx, 0
.loop:
	mov dl, byte [rdi + rcx]
	mov byte [rsi + rcx], dl
	inc rcx
	cmp rcx, rax
	jna .loop
	pop rdx
	pop rsi
	pop rdi
	ret
.end:
	mov rax, 0	
	pop rdx
	pop rsi
	pop rdi
    ret
