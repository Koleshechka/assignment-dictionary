%define LABEL 0
%macro colon 2

    %if LABEL == 0
        %2: dq 0
    %else 
        %2: dq LABEL
    %endif
    db %1, 0
    %define LABEL %2


%endmacro
